export default {
  saucer: {
    movement: {
      FLY_IN: {
        x: '-835px',
        ease: 'power1.out',
        duration: 2,
        id: 'flyIn',
      },
      FLY_AWAY: {
        x: '-1800px',
        duration: 1.5,
        ease: 'power1.in',
        id: 'flyOut',
      },
    },
    beam: {
      FLY_IN: {
        duration: 1.2,
        opacity: 0.6,
        idTop: 'showTopBeam',
        idBottom: 'showBottomBeam',
      },
      FLY_AWAY: {
        duration: 0.8,
        opacity: 0,
        idTop: 'hideTopBeam',
        idBottom: 'hideBottomBeam',
      },
    },
  },
  cow: {
    movement: {
      duration: 2,
      rotation: 20,
      y: '-390px',
      ease: 'power1.out',
      id: 'cowAbduction',
    },
    hide: {
      duration: 0,
      opacity: 0,
      id: 'cowHide',
    },
  },
};
