import gsap from 'gsap/all';
import EventEmitter from 'eventemitter3';

const EVENTS = {
  ABDUCT_COMPLETED: 'abduct_completed',
};

export default class Cow extends EventEmitter {
  constructor({ cowElement, config }) {
    super();

    this._cowElement = cowElement;
    this._config = config;
  }

  static get events() {
    return EVENTS;
  }

  async moveTo() {
    const { duration, rotation, y, ease, id } = this._config.movement;

    await gsap.to(this._cowElement, { duration, rotation, y, ease, id });

    this.emit(Cow.events.ABDUCT_COMPLETED);
  }

  hide() {
    const { duration, id, opacity } = this._config.hide;

    gsap.to(this._cowElement, { duration, opacity, id });
  }
}
