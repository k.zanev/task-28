import Saucer from './Saucer';
import Cow from './Cow';
import config from '../../config';

export default class Animation {
  constructor({ saucerSelector, beamTopSelector, beamBottomSelector, cowSelector }) {
    this.saucer = new Saucer({
      saucerElement: this._getElement(saucerSelector),
      beamTopElement: this._getElement(beamTopSelector),
      beamBottomElement: this._getElement(beamBottomSelector),
      config: config.saucer,
    });

    this.cow = new Cow({
      cowElement: this._getElement(cowSelector),
      config: config.cow,
    });
  }

  async start() {
    await this._saucerFlyIn();

    await this._cowAbduction();

    await this._saucerFlyAway();
  }

  async _saucerFlyIn() {
    const type = 'FLY_IN';

    await this.saucer.moveTo(type);
    await this.saucer.toggleBeam(type);
  }

  async _saucerFlyAway() {
    const type = 'FLY_AWAY';

    await this.saucer.toggleBeam(type);
    await this.saucer.moveTo(type);
  }

  async _cowAbduction() {
    await this.cow.moveTo();
    this.cow.hide();
  }

  _getElement(selector) {
    return document.querySelector(selector);
  }
}
