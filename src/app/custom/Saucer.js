import gsap from 'gsap/all';
import EventEmitter from 'eventemitter3';

const EVENTS = {
  FLY_IN: 'fly_in',
  FLY_AWAY: 'fly_away',
  BEAM_SHOW: 'beam_showed',
  BEAM_HIDE: 'beam_hide',
};

export default class Saucer extends EventEmitter {
  constructor({ saucerElement, beamTopElement, beamBottomElement, config }) {
    super();

    this._saucerElement = saucerElement;
    this._beamTopElement = beamTopElement;
    this._beamBottomElement = beamBottomElement;
    this._config = config;
  }

  static get events() {
    return EVENTS;
  }

  async moveTo(type) {
    const { duration, x, ease, id } = this._config.movement[type];

    await gsap.to(this._saucerElement, { duration, x, ease, id });

    this.emit(Saucer.events[type]);
  }

  async toggleBeam(type) {
    const { duration, opacity, idTop, idBottom } = this._config.beam[type];

    await Promise.all([
      gsap.to(this._beamBottomElement, { duration, opacity, id: idBottom }),
      gsap.to(this._beamTopElement, { duration, opacity, id: idTop }),
    ]);

    this.emit(opacity === 0 ? Saucer.events.BEAM_HIDE : Saucer.events.BEAM_SHOW);
  }
}
